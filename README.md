[Ionic](http://ionicframework.com/docs/) projects.

### Instalar nvm primeiro

[NVM](https://github.com/coreybutler/nvm-windows/releases)
O nvm gerencia varias versões do node em um mesmo ambiente.

Obs. 1 - toda vez que instala algo é aconselhavel reiniciar o terminal[no windows](pro sistema enxergar que as variaveis de ambiente que foram atualizadas).

Obs. 2 - As vezes é aconselhavel rodar o cmd, como modo administrador.

Para testar a instalação e depois instalar e usar:
```bash
$ nvm
$ nvm list available
$ nvm install <numero versao - eu uso a stable>
$ nvm use <numero versao - eu uso a stable>
```
### Node
O [NODE.js](https://nodejs.org/en/) é responsavel por executar esses frameworks ele é tipo um interpretador javascript ultra poderoso!<br>
Mas não precisamos instalar manualmente, pois já foi feito através do NVM.<br>
Mas vamos verificar que versões estão em utilização.
```bash
$ node -v
$ npm -v
```
### With the Ionic CLI:

Instalando globalmente(-g)[isso pode requerer o sudo] o Ionic e o Cordova
```bash
$ npm install -g ionic cordova
```

Then, to run it, cd into `IonInelegiveis` and run:<br>
obs.: Alguns comandos rodam melhor no cmd|bash puro
```bash
$ ionic serve --lab
$ ionic serve
```
só buildar:
```bash
$ ionic cordova build <destino>
$ ionic cordova build android
```

para executar no celular:
```bash
$ ionic cordova run <destino>
$ ionic cordova run android
```
<destino> = android, ios, etc...

